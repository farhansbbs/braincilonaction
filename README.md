# Tugas Kelompok 1 : Kelompok B04

## BOA : Braincil On Action

## Daftar isi
- [Daftar Isi](#daftar-isi)
- [Nama-nama Anggota Kelompok](#nama-anggota-kelompok)
- [Link Herokuapp](#link-herokuapp)
- [Cerita Aplikasi yang Diajukan Serta Kebermanfaatannya](#cerita-aplikasi)
- [Daftar Fitur yang Akan Diaplikasikan](#fitur)
- [Badges]

## Nama Anggota Kelompok

1. Annisa Devi Nurmalasari - 1906292894
2. Michael Eliazer - 1806191124
3. Anis Farhan Itsnaini - 1906285541
4. Julian Fernando - 1906285522

## Link Herokuapp
shell
http://braincilonaction.herokuapp.com/

## Cerita Aplikasi yang Diajukan Serta Kebermanfaatannya
BOA:Braincil On Action dibuat dalam rangka menyediakan wadah bagi pelajar yang bertanya-tanya mengenai sesuatu tapi tidak tahu di mana dia harus menuangkannya, padahal sebelum dia bertanya dia tidak pernah tahu apakah pertanyaannya itu bisa mengubah dunia. 

Mengapa harus braincil padahal sudah banyak website tanya-jawab semacamnya? 
1. Braincil dibuat tidak hanya fokus pada materi akademik yang ditanyakan siswa namun juga menyediakan fitur khusus melayani curhatan akademik mahasiswa. 

2. Braincil didesain secara khusus baik secara fungsional maupun estetika dengan memperhatikan suasana yang tepat untuk sarana tanya-jawab siswa.

3. Braincil tidak hanya berorientasi pada kemudahan untuk siswa tapi juga untuk pada guru (pemberi pertanyaan) dan orang tua wali murid.

Harapannya, dengan dibuatnya website braincil ini, siswa tidak lagi kebingungan menuangkan pertanyaan-pertanyaan berharga. Siswa yang cenderung tertutup juga bisa menuangkan curhatan akademik nya di website ini. 

## Fitur
1. Homepage
-About Braincil
-Tampilan tanggapan mereka tentang Braincil
-Sekilas tampilan perspektif siswa dan guru
2. Halaman Log In
3. Halaman Regist
4. Halaman Pertanyaan 
-Terbagi menjadi beberapa mata kuliah dan curhat akademik
5. Halaman Jawaban 
-Halaman solusi terkait soal yang dituju

## Badges

**Status pipelines:**
[![pipeline status](https://gitlab.com/kelompokppwb04/braincilonaction/badges/master/pipeline.svg)](https://gitlab.com/kelompokppwb04/braincilonaction/commits/master)

**Status code coverage:**
[![coverage report](https://gitlab.com/kelompokppwb04/braincilonaction/badges/master/coverage.svg)](https://gitlab.com/kelompokppwb04/braincilonaction/commits/master)

